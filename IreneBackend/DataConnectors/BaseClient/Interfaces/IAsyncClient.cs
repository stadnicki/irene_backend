﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace DPL.DataConnectors.BaseClient.Interfaces
{
    public interface IAsyncClient<TConnectorOptions>
    {
        Task LoginAsync();

        Task<JObject> QueryAsync(string soqlQuery);

        Task<JObject> QueryMoreResultsAsync(string nextRecordsetUrl);
    }
}
