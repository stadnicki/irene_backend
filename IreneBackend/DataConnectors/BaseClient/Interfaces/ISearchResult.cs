﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DPL.DataConnectors.BaseClient.Interfaces
{
    public interface ISearchResult
    {
        long TotalSize { get; }
        List<string> Records { get; }
        bool HasMoreRows { get; }
        Task FetchMore();
    }
}
