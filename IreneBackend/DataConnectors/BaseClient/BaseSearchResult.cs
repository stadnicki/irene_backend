﻿using DPL.DataConnectors.BaseClient.Interfaces;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DPL.DataConnectors.BaseClient
{
    public abstract class BaseSearchResult<TConnectorOptions> : ISearchResult
    {
        public virtual long TotalSize { get; protected set; }
        public virtual List<string> Records { get; protected set; }
        public virtual bool HasMoreRows { get; protected set; }

        protected IAsyncClient<TConnectorOptions> Client { get; set; }

        public abstract Task FetchMore();

        protected abstract void ParseResults(JObject salesForceResult);

        public BaseSearchResult(JObject salesForceResult, IAsyncClient<TConnectorOptions> client)
        {
            Client = client;
        }
    }
}
