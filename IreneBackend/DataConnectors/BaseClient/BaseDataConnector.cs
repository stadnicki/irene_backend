﻿using DPL.DataConnectors.BaseClient.Interfaces;
using Microsoft.Extensions.Options;

namespace DPL.DataConnectors.BaseClient
{
    public class BaseDataConnector<TConnectorOptions> where TConnectorOptions: BaseConnectorOptions, new()
    {
        protected IAsyncClient<TConnectorOptions> Client { get; set; }
        protected TConnectorOptions Options { get; set; }

        public BaseDataConnector(IAsyncClient<TConnectorOptions> client, IOptions<TConnectorOptions> options)
        {
            Client = client;
            Options = options.Value;
        }
    }
}
