﻿namespace DPL.DataConnectors.BaseClient
{
    public abstract class BaseConnectorOptions
    {
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
    }
}
