﻿using System.Threading.Tasks;
using DPL.DataConnectors.BaseClient.Interfaces;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace DPL.DataConnectors.BaseClient
{
    public abstract class BaseClient<TConnectorOptions> : IAsyncClient<TConnectorOptions> where TConnectorOptions: BaseConnectorOptions, new()
    {
        protected TConnectorOptions Options;

        public BaseClient(IOptions<TConnectorOptions> options)
        {
            Options = options.Value;
        }

        public abstract Task LoginAsync();
        public abstract Task<JObject> QueryAsync(string soqlQuery);
        public abstract Task<JObject> QueryMoreResultsAsync(string nextRecordsetUrl);

    }
}
