﻿using System.Net.Http;
using Microsoft.Extensions.Options;

namespace DPL.DataConnectors.BaseClient
{
    public abstract class BaseHttpClient<TConnectorOptions> : BaseClient<TConnectorOptions> where TConnectorOptions: BaseConnectorOptions, new()
    {
        protected HttpClientHandler ClientHandler;
        protected HttpClient Client;

        public BaseHttpClient(IOptions<TConnectorOptions> options): base(options)
        {
            ClientHandler = CreateClientHandler();
            Client = new HttpClient(ClientHandler);
        }

        public virtual HttpClientHandler CreateClientHandler()
        {
            return new HttpClientHandler();
        }

    }
}
