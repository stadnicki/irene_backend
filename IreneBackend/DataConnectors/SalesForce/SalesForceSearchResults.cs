﻿using DPL.DataConnectors.BaseClient;
using DPL.DataConnectors.BaseClient.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DPL.DataConnectors.SalesForce
{
    public class SalesForceSearchResults: BaseSearchResult<SalesForceConnectorOptions>
    {
        protected string _nextRecordsUrl;

        public SalesForceSearchResults(JObject salesForceResult, IAsyncClient<SalesForceConnectorOptions> client): base(salesForceResult, client)
        {
            ParseResults(salesForceResult);
        }

        public override async Task FetchMore()
        {
            if (!HasMoreRows)
            {
                throw new InvalidOperationException("No more records available");
            }

            var results = await Client.QueryMoreResultsAsync(_nextRecordsUrl);
            ParseResults(results);
        }

        protected override void ParseResults(JObject salesForceResult)
        {
            TotalSize = salesForceResult.Value<long>("totalSize");
            _nextRecordsUrl = salesForceResult["nextRecordsUrl"] != null ? salesForceResult.Value<string>("nextRecordsUrl") : null;

            var records = salesForceResult.SelectToken("$.records");
            Records = new List<string>();

            foreach (var record in records.Children())
            {
                Records.Add(record.ToString());
            }
        }
    }
}
