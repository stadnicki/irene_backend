﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using DPL.DataConnectors.BaseClient;
using DPL.DataConnectors.BaseClient.Interfaces;

namespace DPL.DataConnectors.SalesForce
{
    public class SalesForceClient: BaseHttpClient<SalesForceConnectorOptions>, IAsyncClient<SalesForceConnectorOptions>
    {

        protected string _authToken;
        protected string _instanceUrl;

        public SalesForceClient(IOptions<SalesForceConnectorOptions> options): base(options)
        {
        }

        public override HttpClientHandler CreateClientHandler()
        {
            return new HttpClientHandler
            {
                SslProtocols = System.Security.Authentication.SslProtocols.Tls12
            };
        }

        public async override Task LoginAsync()
        {
            using (var handler = new HttpClientHandler())
            {
                handler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;

                using (var client = new HttpClient(handler))
                {
                    var request = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        {"grant_type", "password"},
                        {"client_id", Options.ClientId},
                        {"client_secret", Options.ClientSecret},
                        {"username", Options.Username},
                        {"password", Options.Password + Options.Token}
                    }
                    );

                    request.Headers.Add("X-PrettyPrint", "1");

                    var response = await client.PostAsync(Options.LoginEndpoint, request);
                    var jsonResponse = await response.Content.ReadAsStringAsync();

                    var authVals = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonResponse);
                    _authToken = authVals["access_token"];
                    _instanceUrl = authVals["instance_url"];
                }
            }
        }

        public async override Task<JObject> QueryAsync(string soqlQuery)
        {
            var queryString = $"/query/?q={soqlQuery}";

            return await DoRequestAsync(queryString);
        }

        public async override Task<JObject> QueryMoreResultsAsync(string nextRecordsetUrl)
        {
            var queryString = $"{_instanceUrl}{Options.ApiEndpoint}{nextRecordsetUrl}";
            return await DoRequestAsync(queryString);
        }

        protected async Task<JObject> DoRequestAsync(string queryString)
        {
            if (_authToken == null)
            {
                await LoginAsync();
            }

            var requestUrl = $"{_instanceUrl}{Options.ApiEndpoint}{queryString}";
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

            request.Headers.Add("Authorization", $"Bearer {_authToken}");
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            request.Headers.Add("X-PrettyPrint", "1");

            var response = await Client.SendAsync(request);
            var responseString = await response.Content.ReadAsStringAsync();

            var resultObject = JObject.Parse(responseString);

            return resultObject;
        }
    }
}
