﻿using DPL.DataConnectors.BaseClient;

namespace DPL.DataConnectors.SalesForce
{
    public class SalesForceConnectorOptions: BaseConnectorOptions
    {
        public string LoginEndpoint { get; set; } = "https://login.salesforce.com/services/oauth2/token";
        public string ApiEndpoint { get; set; } = "/services/data/v36.0";

        public string Token { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public string AccountsQuery { get; set; }
    }
}
