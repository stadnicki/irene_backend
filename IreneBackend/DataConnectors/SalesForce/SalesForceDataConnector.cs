﻿using Microsoft.Extensions.Options;
using System.Threading.Tasks;

using DPL.DataConnectors.BaseClient;

namespace DPL.DataConnectors.SalesForce
{
    public class SalesForceDataConnector: BaseDataConnector<SalesForceConnectorOptions>
    {

        public SalesForceDataConnector(SalesForceClient client, IOptions<SalesForceConnectorOptions> options): base(client, options)
        {
        }

        public async Task<SalesForceSearchResults> GetAccountsAsync()
        {
            var query = Options.AccountsQuery;
            var sfJson = await Client.QueryAsync(query);
            return new SalesForceSearchResults(sfJson, Client);
        }
    }
}
