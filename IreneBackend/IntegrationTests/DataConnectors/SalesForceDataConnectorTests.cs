﻿using DPL.DataConnectors.SalesForce;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.DataConnectors
{
    public class SalesForceDataConnectorTests
    {
        private SalesForceConnectorOptions _connectionOpts = new SalesForceConnectorOptions
        {
            Username = "tech@datapractitioners.org",
            Password = "1q2w#E$R",
            ClientId = "3MVG9fTLmJ60pJ5IG6CYN85uFCYpVw3DgPMj70Gah4iPui_q0VwMTjAqhFh2U6xohCIQKe8fKv56CuCX.wVDl",
            ClientSecret = "2523183430591959245",
            Token = "PviBvNX7H26rddrjRxVXwEOg",
            AccountsQuery = @"SELECT Id, Name,
                           (SELECT Id, Name, Description FROM Assets),
                           (SELECT Id, Name, Email FROM Contacts)
                        FROM Account"
    };

        private SalesForceDataConnector GetConnector()
        {
            var options = Options.Create<SalesForceConnectorOptions>(_connectionOpts);
            var client = new SalesForceClient(options);
            var connector = new SalesForceDataConnector(client, options);

            return connector;
        }

        [Fact]
        public async Task GetAccountsTest_ShouldReturnData()
        {
            var sut = GetConnector();

            var results = await sut.GetAccountsAsync();
            Assert.NotNull(results);
            Assert.NotNull(results.Records);
            Assert.NotEmpty(results.Records);

            while (results.HasMoreRows)
            {
                await results.FetchMore();
                Assert.NotNull(results.Records);
                Assert.NotEmpty(results.Records);
            }
        }
    }
}
